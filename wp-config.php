<?php
/**
 * The base configurations of the WordPress.
 *
 * This file has the following configurations: MySQL settings, Table Prefix,
 * Secret Keys, and ABSPATH. You can find more information by visiting
 * {@link https://codex.wordpress.org/Editing_wp-config.php Editing wp-config.php}
 * Codex page. You can get the MySQL settings from your web host.
 *
 * This file is used by the wp-config.php creation script during the
 * installation. You don't have to use the web site, you can just copy this file
 * to "wp-config.php" and fill in the values.
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'calloch' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', 'root' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'nN9E^yCKce+K7Y`uF>/%>--mmdc/VoLv+R07^Zi{A!=bzH(`y0|9ld0Dh?C-}-/2');
define('SECURE_AUTH_KEY',  'AOWBlJVC2~@L3dYk/:YVWuar:<b-iEg%,+TpqGD8+6.!BrZ=$dV4tN|&SsvYfxAJ');
define('LOGGED_IN_KEY',    'rVg8c7SVf-f (y|Tou+.~|iO{:cUJM;5!.7$pv82L|q,{+<%fk4S{Mb*[/f2{!+:');
define('NONCE_KEY',        '^68G%OX !IiS{`;ZPW+d^kwnmxiS|l])#qTQ?#!:oK|yRBc-njSm{}CixmPV9QrK');
define('AUTH_SALT',        'YKN{Bd25 ~by`FhK9ozg_xbHvu=<Mwlu.2Hj_xS-6hgRTAGo++X$`>(|}jjdXx^7');
define('SECURE_AUTH_SALT', 's8.+i$,jHSf+q4]nJJ>bI/w_F{=8#:sWg+[45P+%@cBYn;K9HL[xze^aDC<v98l{');
define('LOGGED_IN_SALT',   '<o(=}bv+N+p+43sxN{?n}dHTnW8b{YvIV#=U|z/zNJZ9<i-%fM=p3Lkw},9/LPDf');
define('NONCE_SALT',       'pK_g/d8+rLwZS$>O<8CCqyRz!|xFxW`kxRO^|*WqOde,lh{{)#Q*{-Vtz_bwa+n,');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each a unique
 * prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
